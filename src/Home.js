import React from 'react';
import ReactDOM from 'react-dom';

import './Home.css';

function Search () {
    return (
        <label>
            <input className="LabelSearch"
                type="text"
            />
        </label>
    );
}

function Model() {
    return (
        <h6>Nouveau modèle de vin</h6>
    );
}

function Field(props) {
    return (
        <input
            className={props.className}
            type={props.type}
            placeholder={props.value}
            list={props.idList}
            onBlur={props.onChange}
        />
    );
}

class Form extends React.Component {
    constructor(props) {
        super(props);
    }

    renderField(classn, accept, pHolder, id) {
        return (
            <Field 
                type={accept}
                value={pHolder}
                idList={id}
                className={classn}
                onChange={this.props.fieldChange}
            />
        );
    }
    render() {
        return (
            <form>
                <div>
                    {this.renderField("photo", "image","","")}<br/>
                </div>
                {this.renderField("Field","text","PRODUCTEUR/MARQUE","")}<br/>
                {this.renderField("Field","text","NOM DU VIN","")}<br/>
                {this.renderField("Field","text","MILLÉSIME","")}
                <h6>CONTENANCES POSSIBLES</h6>
                <label className="check">
                    {this.renderField("","checkbox","","")}
                    75cL
                </label>
                <label className="check">
                    {this.renderField("","checkbox","","")}
                    1,5L
                </label>
                <label className="check">
                    {this.renderField("","checkbox","","")}
                    3L
                </label>
                <p></p>
                {this.renderField("bigField",
                                "text",
                                "INFORMATIONS PRODUIT (déclaratives)","")}
            </form>
        );
    }
}

function Overview (props) {
    return(
       <div>
            <p>{props.text}</p>
        </div>
    );
}

class Home extends React.Component {
    constructor() {
        super();
        this.state = {
            val : "Unknown",
        }
    }
    renderSearch() {
        return (
            <Search/>
        );
    }

    handleFieldChange () {
        //return; 
        //this.state.val = "BOOM";
    }

    render() {
        return (
            <div className="Page">
                <div className="models">
                    {this.renderSearch()}
                    {Model()}
                </div>
                <div className="formdiv">
                    <Form fieldChange={this.handleFieldChange()}/>
                </div>
                <div className="overview">
                    <Overview text={this.state.val}/> 
                </div>
            </div>
        )
    }
}

export default Home;