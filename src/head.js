import React from 'react';
import logo from './logo.svg';

import './Head.css';

function Head() {
    return (
        <div className="myHeader">
            <img src={logo} className="logoH" alt="logo" />
        </div>
    );
}

export default Head;