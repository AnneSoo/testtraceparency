import React from 'react';
import logo from './logo.svg';
import './App.css';

import Head from './head';
import Navbar from './Navbar';

function App() {
  return (
    <div className="App">
      <Head />
      <Navbar />
      <body className="App-body">
      </body>
    </div>
  );
}

export default App;
