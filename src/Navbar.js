import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Link } from 'react-router-dom';

import './Navbar.css';

import Home from './Home';
import About from './About';

function Navbar() {
    return(
        <Router className="Bar">
            <nav className="Navi">
                <ul className="texts">
                    <Link to='/'> Home </Link><br/>
                    <Link to='/About'> About </Link>
                </ul>
            </nav>
            <div>
                <Route exact path="/" component={Home}/>
                <Route exact path="/About" component={About}/>
            </div>
        </Router>
    );
}

export default Navbar;